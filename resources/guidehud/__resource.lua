client_script 'client/client.lua'
-- Tell server we will close the loading screen resource ourselfs
loadscreen_manual_shutdown "yes"


ui_page 'HUD/ui.html'
loadscreen 'LoadingScreen/ui.html'

files {
  'config.js',
  'LoadingScreen/ui.html',
  'HUD/ui.html',
  'assets/css/style.css',
  'assets/img/cursor.png',
  'assets/img/discord.png',
  'assets/img/home.png',
  'assets/img/logo.png',
  'assets/js/app.js',
  'assets/js/music.js'
}