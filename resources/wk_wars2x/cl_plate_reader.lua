--[[-----------------------------------------------------------------------

	Wraith ARS 2X
	Created by WolfKnight

-----------------------------------------------------------------------]]--

READER = {}

--[[----------------------------------------------------------------------------------
	Plate reader variables

	NOTE - This is not a config, do not touch anything unless you know what
	you are actually doing. 
----------------------------------------------------------------------------------]]--
READER.vars = 
{
    -- Whether or not the plate reader's UI is visible 
	displayed = false,

	-- Whether or not the plate reader should be hidden, e.g. the display is active but the player then steps
	-- out of their vehicle
    hidden = false,

    -- The BOLO plate 
    boloPlate = "", 
    
    -- Cameras, this table contains all of the data needed for operation of the front and rear plate reader 
    cams = {
        -- Variables for the front camera
        ["front"] = {
            plate = "",     -- The current plate caught by the reader
            index = "",     -- The index of the current plate
            locked = false  -- If the reader is locked 
        }, 

        -- Variables for the rear camera
        ["rear"] = {
            plate = "",     -- The current plate caught by the reader
            index = "",     -- The index of the current plate
            locked = false  -- If the reader is locked 
        }
    }
}

-- Gets the display state
function READER:GetDisplayState()
	return self.vars.displayed
end 

-- Toggles the display state of the plate reader system
function READER:ToggleDisplayState()
	-- Toggle the display variable 
	self.vars.displayed = not self.vars.displayed 

	-- Send the toggle message to the NUI side 
	SendNUIMessage( { _type = "setReaderDisplayState", state = self:GetDisplayState() } )
end 

-- Sets the display's hidden state to the given state 
function READER:SetDisplayHidden( state )
	self.vars.hidden = state 
end 

-- Returns if the display is hidden 
function READER:GetDisplayHidden()
	return self.vars.hidden 
end

-- Returns the stored plate for the given reader
function READER:GetPlate( cam )
    return self.vars.cams[cam].plate 
end 

-- Sets the plate for the given reader to the given plate 
function READER:SetPlate( cam, plate )
    self.vars.cams[cam].plate = plate 
end 

-- Returns the stored plate index for the given reader
function READER:GetIndex( cam )
    return self.vars.cams[cam].index
end 

-- Sets the plate index for the given reader to the given index
function READER:SetIndex( cam, index )
    self.vars.cams[cam].index = index 
end 

-- Returns the bolo plate
function READER:GetBoloPlate()
    return self.vars.boloPlate
end 

-- Sets the bolo plate to the given plate 
function READER:SetBoloPlate( plate )
    self.vars.boloPlate = plate 
    UTIL:Notify( "BOLO plate set to: " .. plate )
end 

-- Returns if the given reader is locked
function READER:GetCamLocked( cam )
    return self.vars.cams[cam].locked
end 

-- Locks the given reader
function READER:LockCam( cam )
    -- Check that plate readers can actually be locked
    if ( PLY:VehicleStateValid() and self:CanPerformMainTask() ) then 
        -- Toggle the lock state 
        self.vars.cams[cam].locked = not self.vars.cams[cam].locked

        -- Tell the NUI side to show/hide the lock icon 
        SendNUIMessage( { _type = "lockPlate", cam = cam, state = self:GetCamLocked( cam ) } )

        -- Play a beep 
        SendNUIMessage( { _type = "audio", name = "beep", vol = RADAR:GetSettingValue( "beep" ) } )
    end 
end 

-- Returns if the plate reader system can perform tasks
function READER:CanPerformMainTask()
    return self.vars.displayed and not self.vars.hidden
end 

-- Returns if the given relative position value is for front or rear 
function READER:GetCamFromNum( relPos )
    if ( relPos == 1 ) then 
        return "front"
    elseif ( relPos == -1 ) then 
        return "rear"
    end 
end 

-- Runs when the "Toggle Display" button is pressed on the plate reder box 
RegisterNUICallback( "togglePlateReaderDisplay", function()
	-- Toggle the display state 
	READER:ToggleDisplayState()
end )

-- Runs when the "Set BOLO Plate" button is pressed on the plate reader box
RegisterNUICallback( "setBoloPlate", function( plate, cb )
    -- Set the BOLO plate 
    READER:SetBoloPlate( plate )
end )

-- This is the main function that runs and scans all vehicles in front and behind the patrol vehicle
function READER:Main()
    -- Check that the system can actually run 
    if ( PLY:VehicleStateValid() and self:CanPerformMainTask() ) then 
        -- Loop through front (1) and rear (-1)
        for i = 1, -1, -2 do 
            -- 
            local start = GetEntityCoords( PLY.veh )
            local offset = GetOffsetFromEntityInWorldCoords( PLY.veh, 0.0, ( 40.0 * i ), 0.0 )
            local veh = UTIL:GetVehicleInDirection( PLY.veh, start, offset )

            local cam = self:GetCamFromNum( i )
            
            if ( DoesEntityExist( veh ) and IsEntityAVehicle( veh ) and not self:GetCamLocked( cam ) ) then 
                local plate = GetVehicleNumberPlateText( veh )
                local index = GetVehicleNumberPlateTextIndex( veh )

                if ( self:GetPlate( cam ) ~= plate ) then 
                    self:SetPlate( cam, plate )
                    self:SetIndex( cam, index )

                    if ( plate == self:GetBoloPlate() ) then 
                        self:LockCam( cam )
                    end 

                    SendNUIMessage( { _type = "changePlate", cam = cam, plate = plate, index = index } )
                end 
            end 
        end 
    end 
end 

Citizen.CreateThread( function()
    while ( true ) do
        READER:Main()

        Citizen.Wait( 500 )
    end 
end )

function READER:RunDisplayValidationCheck()
	if ( ( ( PLY.veh == 0 or ( PLY.veh > 0 and not PLY.vehClassValid ) ) and self:GetDisplayState() and not self:GetDisplayHidden() ) or IsPauseMenuActive() and self:GetDisplayState() ) then
		self:SetDisplayHidden( true ) 
		SendNUIMessage( { _type = "setReaderDisplayState", state = false } )
	elseif ( PLY.veh > 0 and PLY.vehClassValid and PLY.inDriverSeat and self:GetDisplayState() and self:GetDisplayHidden() ) then 
		self:SetDisplayHidden( false ) 
		SendNUIMessage( { _type = "setReaderDisplayState", state = true } )
	end 
end

Citizen.CreateThread( function() 
	Citizen.Wait( 100 )

	while ( true ) do 
		READER:RunDisplayValidationCheck()

		Citizen.Wait( 500 )
	end 
end )