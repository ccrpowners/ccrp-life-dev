Config                            = {}

Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }

Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = false
Config.EnableESXIdentity          = true -- enable if you're using esx_identity
Config.EnableNonFreemodePeds      = true -- turn this on if you want custom peds
Config.EnableLicenses             = false -- enable if you're using esx_license

Config.EnableHandcuffTimer        = true -- enable handcuff timer? will unrestrain player after the time ends
Config.HandcuffTimer              = 10 * 60000 -- 10 mins

Config.EnableJobBlip              = true -- enable blips for colleagues, requires esx_society

Config.MaxInService               = -1
Config.Locale                     = 'en'

Config.PoliceStations = {

	LSPD = {

		Blip = {
			Coords  = vector3(425.1, -979.5, 30.7),
			Sprite  = 60,
			Display = 4,
			Scale   = 1.2,
			Colour  = 29
		},

		Cloakrooms = {
			vector3(452.6, -992.8, 30.6)
		},

		Armories = {
			vector3(453.55, -980.1, 30.01)
		},

		Vehicles = {
			{
				Spawner = vector3(454.6, -1017.4, 28.4),
				InsideShop = vector3(228.5, -993.5, -99.5),
				SpawnPoints = {
					{ coords = vector3(438.4, -1018.3, 27.7), heading = 90.0, radius = 6.0 },
					{ coords = vector3(441.0, -1024.2, 28.3), heading = 90.0, radius = 6.0 },
					{ coords = vector3(453.5, -1022.2, 28.0), heading = 90.0, radius = 6.0 },
					{ coords = vector3(450.9, -1016.5, 28.1), heading = 90.0, radius = 6.0 }
				}
			},

			{
				Spawner = vector3(473.3, -1018.8, 28.0),
				InsideShop = vector3(228.5, -993.5, -99.0),
				SpawnPoints = {
					{ coords = vector3(475.9, -1021.6, 28.0), heading = 276.1, radius = 6.0 },
					{ coords = vector3(484.1, -1023.1, 27.5), heading = 302.5, radius = 6.0 }
				}
			}
		},

		Helicopters = {
			{
				Spawner = vector3(461.1, -981.5, 43.6),
				InsideShop = vector3(477.0, -1106.4, 43.0),
				SpawnPoints = {
					{ coords = vector3(449.5, -981.2, 43.6), heading = 92.6, radius = 10.0 }
				}
			}
		},

		BossActions = {
			vector3(465.42, -1009.27, 35.93)
		}

	},

	CSP = {

		Blip = {
			Coords  = vector3(377.43, -1613.38, 29.29),
			Sprite  = 60,
			Display = 4,
			Scale   = 1.2,
			Colour  = 29
		},

		Cloakrooms = {
			vector3(382.37, -1610.29, 28.29)
		},

		Armories = {
			vector3(369.36, -1598.85, 28.59)
		},

		Vehicles = {
			{
				Spawner = vector3(373.91, -1617.68, 29.29),
				InsideShop = vector3(228.5, -993.5, -99.5),
				SpawnPoints = {
					{ coords = vector3(373.39, -1624.11, 28.9), heading = 320.50, radius = 6.0 },
					{ coords = vector3(394.73, -1625.74, 28.9), heading = 50.0, radius = 6.0 },
					{ coords = vector3(391.51, -1610.89, 28.9), heading = 230.0, radius = 6.0 },
					{ coords = vector3(385.19, -1634.29, 28.9), heading = 320.50, radius = 6.0 }
				}
			}
		},

		BossActions = {
			vector3(387.47, -1603.94, 28.29)
		}

	},

	SSPD = {

		Blip = {
			Coords  = vector3(1855.23, 3692.56, 39.04),
			Sprite  = 60,
			Display = 4,
			Scale   = 1.2,
			Colour  = 29
		},

		Cloakrooms = {
			vector3(1849.93, 3695.62, 34.26)
		},

		Armories = {
			vector3(1844.99, 3692.08, 33.50)
		},

		Vehicles = {
			{
				Spawner = vector3(1865.42, 3689.53, 34.27),
				InsideShop = vector3(228.5, -993.5, -99.5),
				SpawnPoints = {
					{ coords = vector3(1868.65, 3695.68, 33.19), heading = 210.58, radius = 6.0 },
					{ coords = vector3(1866.1, 3693.91, 33.5), heading = 210.17, radius = 6.0 },
					{ coords = vector3(1861.57, 3707.75, 32.94), heading = 211.5, radius = 6.0 },
					{ coords = vector3(1859.18, 3706.32, 33.08), heading = 209.91, radius = 6.0 }
				}
			}
		},

		BossActions = {
			vector3(1862.16, 3691.76, 34.26)
		}

	},

	PBPD = {

		Blip = {
			Coords  = vector3(-444.2, 6007.77, 40.53),
			Sprite  = 60,
			Display = 4,
			Scale   = 1.2,
			Colour  = 29
		},

		Cloakrooms = {
			vector3(-433.11, 6001.71, 30.72)
		},

		Armories = {
			vector3(-440.44, 5992.46, 30.56)
		},

		Vehicles = {
			{
				Spawner = vector3(-458.95, 6016.64, 31.49),
				InsideShop = vector3(228.5, -993.5, -99.5),
				SpawnPoints = {
					{ coords = vector3(-462.23, 6009.28, 30.95), heading = 266.08, radius = 6.0 },
					{ coords = vector3(-458.05, 6005.28, 30.95), heading = 265.28, radius = 6.0 }
				}
			}
		},

		Helicopters = {
			{
				Spawner = vector3(-451.09, 5985.05, 31.36),
				InsideShop = vector3(477.0, -1106.4, 43.0),
				SpawnPoints = {
					{ coords = vector3(-480.35, 5984.08, 32.18), heading = 315.73, radius = 10.0 }
				}
			}
		},

		BossActions = {
			vector3(-437.55, 5998.23, 30.72)
		}

	}

}

Config.AuthorizedWeapons = {
	recruit = {

	},

	cadet = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	probationary = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	resofficer = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	officer = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	officer1stclass = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},
	seniorofficer = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},
	
	masterofficer = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	corporal = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	sergeant = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	staffsergeant = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},
	
	sergeantmajor = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	lieutenant = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	captain = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	major = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	assischief = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	chief = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	resdeputy = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	deputy = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	deputy1stclass = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	seniordeputy = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	masterdeputy = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	undersheriff = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	deputysheriff = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	sheriff = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	restrooper = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	trooper = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	trooper1stclass = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	seniortrooper = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	mastertrooper = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	ltmajor = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	ltcolonel = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},


	colonel = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	boss = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	dispatcher = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	dispatch = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_VINTAGEPISTOL', price = 0 }
	},

	admin = {
		
	}
}

Config.AuthorizedVehicles = {
	Shared = {
		{ model = 'charger09', label = '09 Charger', price = 1 },
		{ model = 'charger14', label = '14 Charger', price = 1 },
		{ model = 'charger18', label = '18 Charger', price = 1 },
		{ model = 'explorer13', label = '13 Explorer', price = 1 },
		{ model = 'explorer16', label = '16 Explorer', price = 1 },
		{ model = '06tahoe', label = '06 Tahoe', price = 1 },
		{ model = 'police8', label = '4x4 Tahoe', price = 1 },
		{ model = 'tahoe', label = 'Tahoe', price = 1 },
		{ model = 'excursion', label = 'Excursion', price = 1 },
		{ model = 'taurus', label = 'Taurus', price = 1 },
		{ model = 'vic', label = 'CVPI', price = 1 },
		{ model = 'pdbike', label = 'Harley Davidson Electraglide', price = 1 },
		{ model = 'ram', label = '16 Ram', price = 1 }
	},

	recruit = {

	},

	cadet = {

	},

	probationary = {

	},

	resofficer = {

	},

	officer = {

	},

	officer1stclass = {

	},

	seniorofficer = {

	},

	masterofficer = {

	},

	corporal = {

	},

	sergeant = {
		{ model = 'LEO', label = 'Charger Supervisor', price = 1 },
		{ model = 'LEO2', label = 'Taurus Supervisor', price = 1 },
		{ model = 'LEO3', label = 'Explorer Supervisor', price = 1 },
		{ model = 'LEO4', label = 'Expedition Supervisor', price = 1 },
		{ model = 'LEO5', label = 'CVPI Supervisor', price = 1 },
		{ model = 'MRAP', label = 'SRU MRAP', price = 1 }

	},
	
	staffsergeant = {
		{ model = 'LEO', label = 'Charger Supervisor', price = 1 },
		{ model = 'LEO2', label = 'Taurus Supervisor', price = 1 },
		{ model = 'LEO3', label = 'Explorer Supervisor', price = 1 },
		{ model = 'LEO4', label = 'Expedition Supervisor', price = 1 },
		{ model = 'LEO5', label = 'CVPI Supervisor', price = 1 },
		{ model = 'MRAP', label = 'SRU MRAP', price = 1 }

	},

	sergeantmajor = {
		{ model = 'LEO', label = 'Charger Supervisor', price = 1 },
		{ model = 'LEO2', label = 'Taurus Supervisor', price = 1 },
		{ model = 'LEO3', label = 'Explorer Supervisor', price = 1 },
		{ model = 'LEO4', label = 'Expedition Supervisor', price = 1 },
		{ model = 'LEO5', label = 'CVPI Supervisor', price = 1 },
		{ model = 'MRAP', label = 'SRU MRAP', price = 1 }

	},

	lieutenant = {
		{ model = 'LEO', label = 'Charger Supervisor', price = 1 },
		{ model = 'LEO2', label = 'Taurus Supervisor', price = 1 },
		{ model = 'LEO3', label = 'Explorer Supervisor', price = 1 },
		{ model = 'LEO4', label = 'Expedition Supervisor', price = 1 },
		{ model = 'LEO5', label = 'CVPI Supervisor', price = 1 },
		{ model = 'MRAP', label = 'SRU MRAP', price = 1 }

	},

	captain = {
		{ model = 'LEO', label = 'Charger Supervisor', price = 1 },
		{ model = 'LEO2', label = 'Taurus Supervisor', price = 1 },
		{ model = 'LEO3', label = 'Explorer Supervisor', price = 1 },
		{ model = 'LEO4', label = 'Expedition Supervisor', price = 1 },
		{ model = 'LEO5', label = 'CVPI Supervisor', price = 1 },
		{ model = 'MRAP', label = 'SRU MRAP', price = 1 }

	},

	major = {
		{ model = 'LEO', label = 'Charger Supervisor', price = 1 },
		{ model = 'LEO2', label = 'Taurus Supervisor', price = 1 },
		{ model = 'LEO3', label = 'Explorer Supervisor', price = 1 },
		{ model = 'LEO4', label = 'Expedition Supervisor', price = 1 },
		{ model = 'LEO5', label = 'CVPI Supervisor', price = 1 },
		{ model = 'MRAP', label = 'SRU MRAP', price = 1 }

	},
	
	assischief = {
		{ model = 'LEO', label = 'Charger Supervisor', price = 1 },
		{ model = 'LEO2', label = 'Taurus Supervisor', price = 1 },
		{ model = 'LEO3', label = 'Explorer Supervisor', price = 1 },
		{ model = 'LEO4', label = 'Expedition Supervisor', price = 1 },
		{ model = 'LEO5', label = 'CVPI Supervisor', price = 1 },
		{ model = 'MRAP', label = 'SRU MRAP', price = 1 }

	},

	chief = {
		{ model = 'ccpdchief', label = 'Erics Vehicle', price = 1 },
		{ model = 'LEO', label = 'Charger Supervisor', price = 1 },
		{ model = 'LEO2', label = 'Taurus Supervisor', price = 1 },
		{ model = 'LEO3', label = 'Explorer Supervisor', price = 1 },
		{ model = 'LEO4', label = 'Expedition Supervisor', price = 1 },
		{ model = 'LEO5', label = 'CVPI Supervisor', price = 1 },
		{ model = 'MRAP', label = 'SRU MRAP', price = 1 }

	},

	resdeputy = {

	},

	deputy = {

	},

	deputy1stclass = {

	},

	seniordeputy = {

	},

	masterdeputy = {

	},

	undersheriff = {
		{ model = 'LEO', label = 'Charger Supervisor', price = 1 },
		{ model = 'LEO2', label = 'Taurus Supervisor', price = 1 },
		{ model = 'LEO3', label = 'Explorer Supervisor', price = 1 },
		{ model = 'LEO4', label = 'Expedition Supervisor', price = 1 },
		{ model = 'LEO5', label = 'CVPI Supervisor', price = 1 },
		{ model = 'MRAP', label = 'SRU MRAP', price = 1 }

	},

	deputysheriff = {
		{ model = 'LEO', label = 'Charger Supervisor', price = 1 },
		{ model = 'LEO2', label = 'Taurus Supervisor', price = 1 },
		{ model = 'LEO3', label = 'Explorer Supervisor', price = 1 },
		{ model = 'LEO4', label = 'Expedition Supervisor', price = 1 },
		{ model = 'LEO5', label = 'CVPI Supervisor', price = 1 },
		{ model = 'MRAP', label = 'SRU MRAP', price = 1 }

	},

	sheriff = {
		{ model = 'LEO', label = 'Charger Supervisor', price = 1 },
		{ model = 'LEO2', label = 'Taurus Supervisor', price = 1 },
		{ model = 'LEO3', label = 'Explorer Supervisor', price = 1 },
		{ model = 'LEO4', label = 'Expedition Supervisor', price = 1 },
		{ model = 'LEO5', label = 'CVPI Supervisor', price = 1 },
		{ model = 'MRAP', label = 'SRU MRAP', price = 1 }

	},

	restrooper = {

	},

	trooper = {

	},

	trooper1stclass = {

	},

	seniortrooper = {

	},

	mastertrooper = {

	},

	ltmajor = {
		{ model = 'LEO', label = 'Charger Supervisor', price = 1 },
		{ model = 'LEO2', label = 'Taurus Supervisor', price = 1 },
		{ model = 'LEO3', label = 'Explorer Supervisor', price = 1 },
		{ model = 'LEO4', label = 'Expedition Supervisor', price = 1 },
		{ model = 'LEO5', label = 'CVPI Supervisor', price = 1 },
		{ model = 'MRAP', label = 'SRU MRAP', price = 1 }

	},

	ltcolonel = {
		{ model = 'LEO', label = 'Charger Supervisor', price = 1 },
		{ model = 'LEO2', label = 'Taurus Supervisor', price = 1 },
		{ model = 'LEO3', label = 'Explorer Supervisor', price = 1 },
		{ model = 'LEO4', label = 'Expedition Supervisor', price = 1 },
		{ model = 'LEO5', label = 'CVPI Supervisor', price = 1 },
		{ model = 'MRAP', label = 'SRU MRAP', price = 1 }

	},

	colonel = {
		{ model = 'LEO', label = 'Charger Supervisor', price = 1 },
		{ model = 'LEO2', label = 'Taurus Supervisor', price = 1 },
		{ model = 'LEO3', label = 'Explorer Supervisor', price = 1 },
		{ model = 'LEO4', label = 'Expedition Supervisor', price = 1 },
		{ model = 'LEO5', label = 'CVPI Supervisor', price = 1 },
		{ model = 'MRAP', label = 'SRU MRAP', price = 1 }

	},

	boss = {
		{ model = 'sticky', label = 'Stickys Medic', price = 1 },
		{ model = 'ctruck', label = 'Commish Truck', price = 1 },
		{ model = 'k9', label = 'K9', price = 1 },
		{ model = 'dccar', label = 'DeputyCommish car', price = 1 },
		{ model = 'LEO', label = 'Charger Supervisor', price = 1 },
		{ model = 'LEO2', label = 'Taurus Supervisor', price = 1 },
		{ model = 'LEO3', label = 'Explorer Supervisor', price = 1 },
		{ model = 'LEO4', label = 'Expedition Supervisor', price = 1 },
		{ model = 'LEO5', label = 'CVPI Supervisor', price = 1 },
		{ model = 'LEO6', label = 'Chevy Camaro', price = 1 },
		{ model = 'TM', label = 'Tatical Medic', price = 1 },
		{ model = 'MRAP', label = 'SRU MRAP', price = 1 }
	},

	dispatcher = {
	
	},

	dispatch = {
		{ model = 'sheriff2', label = 'F250 Supervisor', price = 1 },
		{ model = 'LEO', label = 'Charger Supervisor', price = 1 },
		{ model = 'LEO2', label = 'Taurus Supervisor', price = 1 },
		{ model = 'LEO3', label = 'Explorer Supervisor', price = 1 },
		{ model = 'LEO4', label = 'Expedition Supervisor', price = 1 },
		{ model = 'LEO5', label = 'CVPI Supervisor', price = 1 }
	},

	admin = {

	}
}

Config.AuthorizedHelicopters = {
	boss = {
		{ model = 'medone', label = 'MedEvac', livery = 0, price = 1 }
	}
}

-- CHECK SKINCHANGER CLIENT MAIN.LUA for matching elements

Config.Uniforms = {
	recruit_wear = {}

}