local ESX = nil
local bombedCars = {}

-- ESX DEPENDANCY --
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

ESX.RegisterUsableItem('engbomb', function(source)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local carJack = xPlayer.getInventoryItem('carjack')
	if Config.JobOnly then
		for i = 1,#Config.Jobs do
			if xPlayer.job.name == Config.Jobs[i] then
				if carJack.count < 1 then
					TriggerClientEvent('esx:showNotification', source, Config.NeedJack)
				else
					TriggerClientEvent('esx_carBomb:getCar', source)
				end
			else
				TriggerClientEvent('esx:showNotification', _source, Config.NeedJob)
			end
		end
	else
		if carJack.count < 1 then
			TriggerClientEvent('esx:showNotification', source, Config.NeedJack)
		else
			TriggerClientEvent('esx_carBomb:getCar', source)
		end
	end
end)

ESX.RegisterUsableItem('speccheck', function(source)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local roll = math.random(50)
	if Config.JobOnly then
		for i = 1,#Config.Jobs do
			if xPlayer.job.name == Config.Jobs[i] then
				if roll == 9 or roll == 24 or roll == 47 then
					TriggerClientEvent('esx:showNotification', _source, Config.BadCheck)
					xPlayer.removeInventoryItem('speccheck', 1)
				else
					TriggerClientEvent('esx_carBomb:checkCar', _source)
				end
			else
				TriggerClientEvent('esx:showNotification', _source, Config.NeedJob)
			end
		end
	else
		if roll == 9 or roll == 24 or roll == 47 then
			TriggerClientEvent('esx:showNotification', _source, Config.BadCheck)
			xPlayer.removeInventoryItem('speccheck', 1)
		else
			TriggerClientEvent('esx_carBomb:checkCar', _source)
		end
	end
end)

ESX.RegisterUsableItem('defuser', function(source)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local roll = math.random(50)
	local carJack = xPlayer.getInventoryItem('carjack')
	if Config.JobOnly then
		for i = 1,#Config.Jobs do
			if xPlayer.job.name == Config.Jobs[i] then
				if carJack.count < 1 then
					TriggerClientEvent('esx:showNotification', _source, Config.NeedJack)
				else
					if roll == 9 or roll == 24 or roll == 47 then
						TriggerClientEvent('esx:showNotification', _source, Config.BadDefus)
						xPlayer.removeInventoryItem('defuser', 1)
					else
						TriggerClientEvent('esx_carBomb:disarmCar', _source)
					end
				end
			else
				TriggerClientEvent('esx:showNotification', _source, Config.NeedJob)
			end
		end
	else
		if roll == 9 or roll == 24 or roll == 47 then
			TriggerClientEvent('esx:showNotification', _source, Config.BadDefus)
			xPlayer.removeInventoryItem('defuser', 1)
		else
			TriggerClientEvent('esx_carBomb:disarmCar', _source)
		end
	end
end)

RegisterServerEvent('esx_carBomb:addPart')
AddEventHandler('esx_carBomb:addPart', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local roll = math.random(30)
	if Config.JobOnly then
		for i = 1,#Config.Jobs do
			if xPlayer.job.name == Config.Jobs[i] then
				if roll == 3 or roll == 17 then
					xPlayer.addInventoryItem('bombpart1', 1)
				elseif roll == 7 or roll == 16 or roll == 24 or roll == 30 then
					xPlayer.addInventoryItem('bombpart2', 1)
				elseif roll == 11 or roll == 20 then
					xPlayer.addInventoryItem('bombpart3', 1)
				elseif roll == 1 then
					xPlayer.addMoney(1)
					TriggerClientEvent('esx:showNotification', _source, Config.GotCoin)
				else
					xPlayer.addInventoryItem('trash', 1)
				end
			end
		end
	else
		if roll == 3 or roll == 17 then
			xPlayer.addInventoryItem('bombpart1', 1)
		elseif roll == 7 or roll == 16 or roll == 24 or roll == 30 then
			xPlayer.addInventoryItem('bombpart2', 1)
		elseif roll == 11 or roll == 20 then
			xPlayer.addInventoryItem('bombpart3', 1)
		elseif roll == 1 then
			xPlayer.addMoney(1)
			TriggerClientEvent('esx:showNotification', _source, Config.GotCoin)
		else
			xPlayer.addInventoryItem('trash', 1)
		end
	end
end)

RegisterServerEvent('esx_carBomb:ensureEsplode')
AddEventHandler('esx_carBomb:ensureEsplode', function(vehicle)
	local xPlayers = ESX.GetPlayers()
	for i = 1,#xPlayers do
		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
		TriggerClientEvent('esx_carBomb:ensureEsplode', xPlayer.source, vehicle)
	end
end)

RegisterServerEvent('esx_carBomb:createBomb')
AddEventHandler('esx_carBomb:createBomb', function(ped, coords)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local part1 = xPlayer.getInventoryItem('bombpart1')
	local part2 = xPlayer.getInventoryItem('bombpart2')
	local part3 = xPlayer.getInventoryItem('bombpart3')
	local bomb = xPlayer.getInventoryItem('engbomb')
	local count = 0
	if Config.JobOnly then
		for i = 1,#Config.Jobs do
			if xPlayer.job.name == Config.Jobs[i] then
				if part1.count >= 1 then
					if part2.count >= Config.WireCost then
						if part3.count >= 1 then
							if Config.Weight then
								if xPlayer.canCarryItem('engbomb', 1) then
									TriggerClientEvent('esx_carBomb:craftBomb', _source, ped, coords)
									xPlayer.removeInventoryItem('bombpart1', 1)
									repeat
										Citizen.Wait(5000)
										xPlayer.removeInventoryItem('bombpart2', 1)
										count = count + 1
									until count == Config.WireCost
									Citizen.Wait(3000)
									xPlayer.removeInventoryItem('bombpart3', 1)
									Citizen.Wait(2000)
									xPlayer.addInventoryItem('engbomb', 1)
								else
									TriggerClientEvent('esx:showNotification', _source, Config.BombFull)
								end
							else
								if bomb.count < bomb.limit then
									TriggerClientEvent('esx_carBomb:craftBomb', _source, ped, coords)
									xPlayer.removeInventoryItem('bombpart1', 1)
									repeat
										Citizen.Wait(5000)
										xPlayer.removeInventoryItem('bombpart2', 1)
										count = count + 1
									until count == Config.WireCost
									Citizen.Wait(3000)
									xPlayer.removeInventoryItem('bombpart3', 1)
									Citizen.Wait(2000)
									xPlayer.addInventoryItem('engbomb', 1)
								else
									TriggerClientEvent('esx:showNotification', _source, Config.BombFull)
								end
							end
						else
							TriggerClientEvent('esx:showNotification', _source, Config.NeedTrig)
						end
					else
						TriggerClientEvent('esx:showNotification', _source, Config.NeedWire)
					end
				else
					TriggerClientEvent('esx:showNotification', _source, Config.NeedCase)
				end
			end
		end
	else
		if part1.count >= 1 then
			if part2.count >= Config.WireCost then
				if part3.count >= 1 then
					if Config.Weight then
						if xPlayer.canCarryItem('engbomb', 1) then
							TriggerClientEvent('esx_carBomb:craftBomb', _source, ped, coords)
							xPlayer.removeInventoryItem('bombpart1', 1)
							repeat
								Citizen.Wait(5000)
								xPlayer.removeInventoryItem('bombpart2', 1)
								count = count + 1
							until count == Config.WireCost
							Citizen.Wait(3000)
							xPlayer.removeInventoryItem('bombpart3', 1)
							Citizen.Wait(2000)
							xPlayer.addInventoryItem('engbomb', 1)
						else
							TriggerClientEvent('esx:showNotification', _source, Config.BombFull)
						end
					else
						if bomb.count < bomb.limit then
							TriggerClientEvent('esx_carBomb:craftBomb', _source, ped, coords)
							xPlayer.removeInventoryItem('bombpart1', 1)
							repeat
								Citizen.Wait(5000)
								xPlayer.removeInventoryItem('bombpart2', 1)
								count = count + 1
							until count == Config.WireCost
							Citizen.Wait(3000)
							xPlayer.removeInventoryItem('bombpart3', 1)
							Citizen.Wait(2000)
							xPlayer.addInventoryItem('engbomb', 1)
						else
							TriggerClientEvent('esx:showNotification', _source, Config.BombFull)
						end
					end
				else
					TriggerClientEvent('esx:showNotification', _source, Config.NeedTrig)
				end
			else
				TriggerClientEvent('esx:showNotification', _source, Config.NeedWire)
			end
		else
			TriggerClientEvent('esx:showNotification', _source, Config.NeedCase)
		end
	end
end)

RegisterServerEvent('esx_carBomb:createDefuser')
AddEventHandler('esx_carBomb:createDefuser', function(ped, coords)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local defus = xPlayer.getInventoryItem('defuser')
	local spec = xPlayer.getInventoryItem('speccheck')
	local bomb = xPlayer.getInventoryItem('engbomb')
	local count = 0
	if Config.JobOnly then
		for i = 1,#Config.Jobs do
			if xPlayer.job.name == Config.Jobs[i] then
				if Config.Weight then
					if xPlayer.canCarryItem('defuser', 1) then
						if bomb.count >= 1 then
							if xPlayer.canCarryItem('speccheck', 1) then
								xPlayer.removeInventoryItem('engbomb', 1)
								xPlayer.addInventoryItem('speccheck', 1)
								xPlayer.addInventoryItem('defuser', 1)
							else
								xPlayer.removeInventoryItem('engbomb', 1)
								xPlayer.addInventoryItem('defuser', 1)
								TriggerClientEvent('esx:showNotification', _source, Config.SpecFull)
							end
						else
							TriggerClientEvent('esx:showNotification', _source, Config.NeedBomb)
						end
					else
						TriggerClientEvent('esx:showNotification', _source, Config.DeffFull)
					end
				else
					if defus.count < defus.limit then
						if bomb.count >= 1 then
							if spec.count < spec.limit then
								xPlayer.removeInventoryItem('engbomb', 1)
								xPlayer.addInventoryItem('speccheck', 1)
								xPlayer.addInventoryItem('defuser', 1)
							else
								xPlayer.removeInventoryItem('engbomb', 1)
								xPlayer.addInventoryItem('defuser', 1)
								TriggerClientEvent('esx:showNotification', _source, Config.HaveSpec)
							end
						else
							TriggerClientEvent('esx:showNotification', _source, Config.NeedBomb)
						end
					else
						TriggerClientEvent('esx:showNotification', _source, Config.HaveDeff)
					end
				end
			end
		end
	else
		if Config.Weight then
			if xPlayer.canCarryItem('defuser', 1) then
				if bomb.count >= 1 then
					if xPlayer.canCarryItem('speccheck', 1) then
						xPlayer.removeInventoryItem('engbomb', 1)
						xPlayer.addInventoryItem('speccheck', 1)
						xPlayer.addInventoryItem('defuser', 1)
					else
						xPlayer.removeInventoryItem('engbomb', 1)
						xPlayer.addInventoryItem('defuser', 1)
						TriggerClientEvent('esx:showNotification', _source, Config.SpecFull)
					end
				else
					TriggerClientEvent('esx:showNotification', _source, Config.NeedBomb)
				end
			else
				TriggerClientEvent('esx:showNotification', _source, Config.DeffFull)
			end
		else
			if defus.count < defus.limit then
				if bomb.count >= 1 then
					if spec.count < spec.limit then
						xPlayer.removeInventoryItem('engbomb', 1)
						xPlayer.addInventoryItem('speccheck', 1)
						xPlayer.addInventoryItem('defuser', 1)
					else
						xPlayer.removeInventoryItem('engbomb', 1)
						xPlayer.addInventoryItem('defuser', 1)
						TriggerClientEvent('esx:showNotification', _source, Config.HaveSpec)
					end
				else
					TriggerClientEvent('esx:showNotification', _source, Config.NeedBomb)
				end
			else
				TriggerClientEvent('esx:showNotification', _source, Config.HaveDeff)
			end
		end
	end
end)

RegisterServerEvent('esx_carBomb:sellTicket')
AddEventHandler('esx_carBomb:sellTicket', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	if Config.JobOnly then
		for i = 1,#Config.Jobs do
			if xPlayer.job.name == Config.Jobs[i] then
				if xPlayer.getMoney() >= Config.TicketCost then
					xPlayer.removeMoney(Config.TicketCost)
					TriggerClientEvent('esx_carBomb:giveTicket', _source)
				else
					TriggerClientEvent('esx:showNotification', _source, Config.NeedCash)
				end
			end
		end
	else
		if xPlayer.getMoney() >= Config.TicketCost then
			xPlayer.removeMoney(Config.TicketCost)
			TriggerClientEvent('esx_carBomb:giveTicket', _source)
		else
			TriggerClientEvent('esx:showNotification', _source, Config.NeedCash)
		end
	end
end)

RegisterServerEvent('esx_carBomb:takeTrash')
AddEventHandler('esx_carBomb:takeTrash', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local _xTrash = xPlayer.getInventoryItem('trash')
	if Config.JobOnly then
		for i = 1,#Config.Jobs do
			if xPlayer.job.name == Config.Jobs[i] then
				if _xTrash.count ~= 0 then
					local rem = _xTrash.count % 5
					xPlayer.removeInventoryItem('trash', (_xTrash.count - rem))
					xPlayer.addMoney(_xTrash.count * (1/5))
				else
					TriggerClientEvent('esx:showNotification', _source, Config.NeedTrsh)
				end
			end
		end
	else
		if _xTrash.count ~= 0 then
			local rem = _xTrash.count % 5
			xPlayer.removeInventoryItem('trash', (_xTrash.count - rem))
			xPlayer.addMoney(_xTrash.count * (1/5))
		else
			TriggerClientEvent('esx:showNotification', _source, Config.NeedTrsh)
		end
	end
end)

RegisterServerEvent('esx_carBomb:triggerBomb')
AddEventHandler('esx_carBomb:triggerBomb', function(ped, coords, id)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	if Config.JobOnly then
		for i = 1,#Config.Jobs do
			if xPlayer.job.name == Config.Jobs[i] then
				xPlayer.removeInventoryItem('engbomb', 1)
				TriggerClientEvent('esx_carBomb:placeBomb', -1, ped, coords, id)
			end
		end
	else
		xPlayer.removeInventoryItem('engbomb', 1)
		TriggerClientEvent('esx_carBomb:placeBomb', -1, ped, coords, id)
	end
end)

RegisterServerEvent('esx_carBomb:storeCars')
AddEventHandler('esx_carBomb:storeCars', function(veh)
	table.insert(bombedCars, veh)
	TriggerEvent('esx_carBomb:sendCars')
end)

RegisterServerEvent('esx_carBomb:removeCars')
AddEventHandler('esx_carBomb:removeCars', function(veh)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local bomb = xPlayer.getInventoryItem('engbomb')
	for k,v in pairs(bombedCars) do
		if veh == v then
			table.remove(bombedCars, k)
		end
	end
	if bomb.count < bomb.limit then
		xPlayer.addInventoryItem('engbomb', 1)
	else
		TriggerClientEvent('esx:showNotification', _source, Config.BombFull)
	end
	TriggerEvent('esx_carBomb:sendCars')
end)

RegisterServerEvent('esx_carBomb:sendCars')
AddEventHandler('esx_carBomb:sendCars', function(veh)
	TriggerClientEvent('esx_carBomb:recieveCars', -1, bombedCars)
end)

RegisterServerEvent('esx_carBomb:policeAlert')
AddEventHandler('esx_carBomb:policeAlert', function(streetName)
	local _source = source
	local xPlayers = ESX.GetPlayers()

	for i=1, #xPlayers, 1 do
		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
		
		if xPlayer.job.name == 'police' then
			TriggerClientEvent('esx:showNotification', xPlayers[i], Config.Intruder .. streetName)
		end
	end
end)