ESX = nil
local hasTicket, fighting, showing, working, warned = false, false, false, false, false
local ticketTime = 0
local enBomCars, poBomCars, msBomCars, stopBomCars, seatBomCars, servBomCars = {}, {}, {}, {}, {}, {}
local npcT = {}
local npc

-- ESX DEPENDANCY --
Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(10)
	end
	
	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end
	
	PlayerData = ESX.GetPlayerData()
	
end)

ObjectInFront = function(ped, pos)
	local entityWorld = GetOffsetFromEntityInWorldCoords(ped, 0.0, 1.5, 0.0)
	local car = CastRayPointToPoint(pos.x, pos.y, pos.z, entityWorld.x, entityWorld.y, entityWorld.z, 30, ped, 0)
	local _, _, _, _, result = GetRaycastResult(car)
	return result
end

OpenDisarmMenu = function(ped, bt)
	local elements = {
		{ label = Config.Confirm, value = 'yes' },
		{ label = Config.Deny, value = 'no' }
	}
	ESX.UI.Menu.CloseAll()
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'disarm', {
		title    = 'Disarm Old Bomb',
		align    = 'top-left',
		elements = elements
	}, function(data, menu)
		if data.current.value == 'yes' then
			if bt == 'eng' then
				enBomCars = {}
			elseif bt == 'pos' then
				poBomCars = {}
			elseif bt == 'speed' then
				msBomCars = {}
			elseif bt == 'stop' then
				stopBomCars = {}
			elseif bt == 'seat' then
				seatBomCars = {}
			end
			menu.close()
		elseif data.current.value == 'no' then
			menu.close()
		end
	end, function(data, menu)
		menu.close()
	end)
end

Search = function(ped, currentpos)
	Citizen.Wait(1000)
	local newpos = GetEntityCoords(ped)
	if newpos ~= currentpos then
		local roll = math.random(50)
		local dict = 'amb@medic@standing@tendtodead@idle_a'
		local view = GetFollowPedCamViewMode()
		RequestAnimDict(dict)
		while not HasAnimDictLoaded(dict) do
			Citizen.Wait(1)
		end
		if view == 4 then
			if roll == 3 or roll == 9 or roll == 15 or roll == 27 or roll == 34 or roll == 49 then
				ESX.ShowNotification(Config.OnGround)
				Citizen.Wait(1000)
				TaskPlayAnim(ped, dict, 'idle_a', 8.0, -8.0, 3000, 1, 0.0, false, false, false)
				Citizen.Wait(2500)
				TriggerServerEvent('esx_carBomb:addPart')
			end
		else
			if roll == 3 or roll == 27 or roll == 49 then
				ESX.ShowNotification(Config.OnGround)
				Citizen.Wait(1000)
				TaskPlayAnim(ped, dict, 'idle_a', 8.0, -8.0, 3000, 1, 0.0, false, false, false)
				Citizen.Wait(2500)
				TriggerServerEvent('esx_carBomb:addPart')
			end
		end
	end
end

RegisterNetEvent('esx_carBomb:recieveCars')
AddEventHandler('esx_carBomb:recieveCars', function(list)
	for i = 1,#list do
		table.insert(servBomCars, list[i])
	end
end)

RegisterNetEvent('esx_carBomb:checkCar')
AddEventHandler('esx_carBomb:checkCar', function()
	local ped = PlayerPedId()
	local coords = GetEntityCoords(ped)
	local veh = ObjectInFront(ped, coords)
	if DoesEntityExist(veh) then
		if IsEntityAVehicle(veh) then
			for k,v in pairs(servBomCars) do
				if veh == v then
					ESX.ShowNotification(Config.HighNit)
				end
			end
		end
	end
end)

RegisterNetEvent('esx_carBomb:disarmCar')
AddEventHandler('esx_carBomb:disarmCar', function()
	local ped = PlayerPedId()
	local coords = GetEntityCoords(ped)
	local veh = ObjectInFront(ped, coords)
	if DoesEntityExist(veh) then
		if IsEntityAVehicle(veh) then
			for k,v in pairs(servBomCars) do
				if veh == v then
					local dict
					local model = 'prop_carjack'
					local offset = GetOffsetFromEntityInWorldCoords(ped, 0.0, -2.0, 0.0)
					local headin = GetEntityHeading(ped)
					FreezeEntityPosition(veh, true)
					local vehpos = GetEntityCoords(veh)
					dict = 'mp_car_bomb'
					RequestAnimDict(dict)
					RequestModel(model)
					while not HasAnimDictLoaded(dict) or not HasModelLoaded(model) do
						Citizen.Wait(1)
					end
					local vehjack = CreateObject(GetHashKey(model), vehpos.x, vehpos.y, vehpos.z - 0.5, true, true, true)
					ESX.ShowNotification(Config.JackLift)
					AttachEntityToEntity(vehjack, veh, 0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, false, false, false, false, 0, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1250, 1, 0.0, 1, 1)
					Citizen.Wait(1250)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.01, true, true, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
					Citizen.Wait(1000)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.025, true, true, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
					Citizen.Wait(1000)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.05, true, true, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
					Citizen.Wait(1000)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.1, true, true, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
					Citizen.Wait(1000)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.15, true, true, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
					Citizen.Wait(1000)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.2, true, true, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
					Citizen.Wait(1000)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.3, true, true, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
					dict = 'move_crawl'
					Citizen.Wait(1000)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.4, true, true, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.5, true, true, true)
					SetEntityCollision(veh, false, false)
					TaskPedSlideToCoord(ped, offset, headin, 1000)
					Citizen.Wait(1000)
					RequestAnimDict(dict)
					while not HasAnimDictLoaded(dict) do
						Citizen.Wait(100)
					end
					ESX.ShowNotification(Config.MoveBomb)
					TaskPlayAnimAdvanced(ped, dict, 'onback_bwd', coords, 0.0, 0.0, headin - 180, 1.0, 0.5, 3000, 1, 0.0, 1, 1)
					dict = 'amb@world_human_vehicle_mechanic@male@base'
					Citizen.Wait(3000)
					RequestAnimDict(dict)
					while not HasAnimDictLoaded(dict) do
						Citizen.Wait(1)
					end
					TaskPlayAnim(ped, dict, 'base', 8.0, -8.0, 5000, 1, 0, false, false, false)
					dict = 'move_crawl'
					Citizen.Wait(5000)
					local coords2 = GetEntityCoords(ped)
					RequestAnimDict(dict)
					while not HasAnimDictLoaded(dict) do
						Citizen.Wait(1)
					end
					if veh == v then
						table.remove(servBomCars, k)
						TriggerServerEvent('esx_carBomb:removeCars', v)
					end
					TaskPlayAnimAdvanced(ped, dict, 'onback_fwd', coords2, 0.0, 0.0, headin - 180, 1.0, 0.5, 2000, 1, 0.0, 1, 1)
					Citizen.Wait(3000)
					dict = 'mp_car_bomb'
					RequestAnimDict(dict)
					while not HasAnimDictLoaded(dict) do
						Citizen.Wait(1)
					end
					ESX.ShowNotification(Config.JackLowr)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1250, 1, 0.0, 1, 1)
					Citizen.Wait(1250)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.4, true, true, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
					Citizen.Wait(1000)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.3, true, true, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
					Citizen.Wait(1000)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.2, true, true, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
					Citizen.Wait(1000)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.15, true, true, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
					Citizen.Wait(1000)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.1, true, true, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
					Citizen.Wait(1000)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.05, true, true, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
					Citizen.Wait(1000)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.025, true, true, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
					dict = 'move_crawl'
					Citizen.Wait(1000)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.01, true, true, true)
					TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
					SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z, true, true, true)
					FreezeEntityPosition(veh, false)
					DetachEntity(vehjack, true, true)
					DeleteEntity(vehjack)
					SetEntityCollision(veh, true, true)
				else
					ESX.ShowNotification(Config.NoDetect)
				end
			end
		end
	end
end)

RegisterNetEvent('esx_carBomb:giveTicket')
AddEventHandler('esx_carBomb:giveTicket', function()
	ticketTime = ticketTime + Config.TicketTime
	ESX.ShowNotification(Config.GotTick ..tonumber(string.format('%.' .. (1) .. 'f', (ticketTime / 60))).. Config.TimeUnit)
end)

RegisterNetEvent('esx_carBomb:craftBomb')
AddEventHandler('esx_carBomb:craftBomb', function(ped, coords)
	local dict
	local headin = GetEntityHeading(ped)
	dict = 'mini@repair'
	RequestAnimDict(dict)
	while not HasAnimDictLoaded(dict) do
		Citizen.Wait(1)
	end
	working = true
	local count = 0
	repeat
		TaskPlayAnimAdvanced(ped, dict, 'fixing_a_ped', coords, 0.0, 0.0, headin, 0.5, 0.01, 5000, 1, 0.0, 1, 1)
		Citizen.Wait(5000)
		count = count + 1
	until count == Config.WireCost
	Citizen.Wait(5000)
	working = false
end)

RegisterNetEvent('esx_carBomb:placeBomb')
AddEventHandler('esx_carBomb:placeBomb', function(ped, coords, id)
	local dict
	local model = 'prop_carjack'
	local headin = GetEntityHeading(ped)
	local veh = NetToVeh(id)
	FreezeEntityPosition(veh, true)
	local vehpos = GetEntityCoords(veh)
	dict = 'mp_car_bomb'
	RequestAnimDict(dict)
	RequestModel(model)
	while not HasAnimDictLoaded(dict) or not HasModelLoaded(model) do
		Citizen.Wait(1)
	end
	local vehjack = CreateObject(GetHashKey(model), vehpos.x, vehpos.y, vehpos.z - 0.5, true, true, true)
	ESX.ShowNotification(Config.JackLift)
	AttachEntityToEntity(vehjack, veh, 0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, false, false, false, false, 0, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1250, 1, 0.0, 1, 1)
	Citizen.Wait(1250)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.01, true, true, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
	Citizen.Wait(1000)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.025, true, true, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
	Citizen.Wait(1000)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.05, true, true, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
	Citizen.Wait(1000)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.1, true, true, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
	Citizen.Wait(1000)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.15, true, true, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
	Citizen.Wait(1000)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.2, true, true, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
	Citizen.Wait(1000)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.3, true, true, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
	dict = 'move_crawl'
	Citizen.Wait(1000)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.4, true, true, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.5, true, true, true)
	SetEntityCollision(veh, false, false)
	TaskPedSlideToCoord(ped, offset, headin, 1000)
	Citizen.Wait(1000)
	RequestAnimDict(dict)
	while not HasAnimDictLoaded(dict) do
		Citizen.Wait(100)
	end
	ESX.ShowNotification(Config.BomPlace)
	TaskPlayAnimAdvanced(ped, dict, 'onback_bwd', coords, 0.0, 0.0, headin - 180, 1.0, 0.5, 3000, 1, 0.0, 1, 1)
	dict = 'amb@world_human_vehicle_mechanic@male@base'
	Citizen.Wait(3000)
	RequestAnimDict(dict)
	while not HasAnimDictLoaded(dict) do
		Citizen.Wait(1)
	end
	TaskPlayAnim(ped, dict, 'base', 8.0, -8.0, 5000, 1, 0, false, false, false)
	dict = 'move_crawl'
	Citizen.Wait(5000)
	local coords2 = GetEntityCoords(ped)
	RequestAnimDict(dict)
	while not HasAnimDictLoaded(dict) do
		Citizen.Wait(1)
	end
	TaskPlayAnimAdvanced(ped, dict, 'onback_fwd', coords2, 0.0, 0.0, headin - 180, 1.0, 0.5, 2000, 1, 0.0, 1, 1)
	Citizen.Wait(3000)
	dict = 'mp_car_bomb'
	RequestAnimDict(dict)
	while not HasAnimDictLoaded(dict) do
		Citizen.Wait(1)
	end
	ESX.ShowNotification(Config.JackLowr)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1250, 1, 0.0, 1, 1)
	Citizen.Wait(1250)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.4, true, true, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
	Citizen.Wait(1000)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.3, true, true, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
	Citizen.Wait(1000)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.2, true, true, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
	Citizen.Wait(1000)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.15, true, true, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
	Citizen.Wait(1000)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.1, true, true, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
	Citizen.Wait(1000)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.05, true, true, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
	Citizen.Wait(1000)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.025, true, true, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
	dict = 'move_crawl'
	Citizen.Wait(1000)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z + 0.01, true, true, true)
	TaskPlayAnimAdvanced(ped, dict, 'car_bomb_mechanic', coords, 0.0, 0.0, headin, 1.0, 0.5, 1000, 1, 0.25, 1, 1)
	SetEntityCoordsNoOffset(veh, vehpos.x, vehpos.y, vehpos.z, true, true, true)
	FreezeEntityPosition(veh, false)
	DetachEntity(vehjack, true, true)
	DeleteEntity(vehjack)
	SetEntityCollision(veh, true, true)
	TriggerServerEvent('esx_carBomb:storeCars', veh)
end)

RegisterNetEvent('esx_carBomb:getCar')
AddEventHandler('esx_carBomb:getCar', function()
	local ped = PlayerPedId()
	local coords = GetEntityCoords(ped)
	local veh = ObjectInFront(ped, coords)
	if DoesEntityExist(veh) then
		if IsEntityAVehicle(veh) then
			local elements = {
				{ label = Config.EngBomb, value = 'engb' },
				{ label = Config.PosBomb, value = 'posb' },
				{ label = Config.SpeBomb, value = 'speedb' },
				{ label = Config.StpBomb, value = 'stopb' },
				{ label = Config.SetBomb, value = 'seatb' }
			}
			ESX.UI.Menu.CloseAll()
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'bombtype', {
				title    = 'Bomb Type',
				align    = 'top-left',
				elements = elements
			}, function(data, menu)
				local bomb = data.current.value
				local id   = VehToNet(veh)

				if bomb == 'engb' then
					if enBomCars[1] == nil then
						enBomCars[1] = veh
						SetEntityAsMissionEntity(veh, true, true)
						TriggerServerEvent('esx_carBomb:triggerBomb', ped, coords, id)
					else
						ESX.ShowNotification(Config.OneBomb)
						OpenDisarmMenu(ped, 'eng')
					end
					menu.close()
				elseif bomb == 'posb' then
					local pos = GetEntityCoords(veh)
					if poBomCars[1] == nil then
						poBomCars[1] = veh
						poBomCars[2] = pos
						SetEntityAsMissionEntity(veh, true, true)
						TriggerServerEvent('esx_carBomb:triggerBomb', ped, coords, id)
						ESX.UI.Menu.Open(
							'dialog', GetCurrentResourceName(), 'choose_detonate_distance',
							{
								title = Config.DetDist
							},
						function(data, menu)

							local detDis = tonumber(data.value)

							if detDis == nil then
								ESX.ShowNotification(Config.DisAboveZero)
							elseif detDis > 1500 then
								ESX.ShowNotification(Config.DisBelow1500)
							else
								poBomCars[3] = detDis
								menu.close()
							end
						end, function(data, menu)
						end)
					else
						ESX.ShowNotification(Config.OneBomb)
						OpenDisarmMenu(ped, 'pos')
					end
					menu.close()
				elseif bomb == 'speedb' then
					local pos = GetEntityCoords(veh)
					if msBomCars[1] == nil then
						msBomCars[1] = veh
						SetEntityAsMissionEntity(veh, true, true)
						TriggerServerEvent('esx_carBomb:triggerBomb', ped, coords, id)
						ESX.UI.Menu.Open(
							'dialog', GetCurrentResourceName(), 'choose_bomb_speed',
							{
								title = Config.DetSpeed
							},
						function(data, menu)

							local bombSpeed = tonumber(data.value)

							if bombSpeed == nil then
								ESX.ShowNotification(Config.SpdAboveZero)
							elseif bombSpeed > 150 then
								ESX.ShowNotification(Config.SpdBelow150)
							else
								msBomCars[2] = bombSpeed
								menu.close()
							end
						end, function(data, menu)
						end)
					else
						ESX.ShowNotification(Config.OneBomb)
						OpenDisarmMenu(ped, 'speed')
					end
					menu.close()
				elseif bomb == 'stopb' then
					local pos = GetEntityCoords(veh)
					if stopBomCars[1] == nil then
						stopBomCars[1] = veh
						stopBomCars[2] = pos
						SetEntityAsMissionEntity(veh, true, true)
						TriggerServerEvent('esx_carBomb:triggerBomb', ped, coords, id)
						ESX.UI.Menu.Open(
							'dialog', GetCurrentResourceName(), 'choose_detonate_distance',
							{
								title = Config.DetDist
							},
						function(data, menu)

							local detDis = tonumber(data.value)

							if detDis == nil then
								ESX.ShowNotification(Config.DisAboveFive)
							elseif detDis <= 5 then
								ESX.ShowNotification(Config.DisAboveFive)
							else
								stopBomCars[3] = detDis
								menu.close()
							end
						end, function(data, menu)
						end)
					else
						ESX.ShowNotification(Config.OneBomb)
						OpenDisarmMenu(ped, 'stop')
					end
					menu.close()
				elseif bomb == 'seatb' then
					if seatBomCars[1] == nil then
						seatBomCars[1] = veh
						SetEntityAsMissionEntity(veh, true, true)
						TriggerServerEvent('esx_carBomb:triggerBomb', ped, coords, id)
						ESX.ShowNotification(Config.SeatList)
						ESX.UI.Menu.CloseAll()
						ESX.UI.Menu.Open(
							'dialog', GetCurrentResourceName(), 'choose_bomb_seat',
							{
								title = Config.TrigBomb
							},
						function(data, menu)

							local seat = data.value

							if seat == nil then
								ESX.ShowNotification(Config.WongSeat)
							elseif seat == 'driver' then
								ESX.ShowNotification(Config.WastBomb)
								seatBomCars[2] = -1
								menu.close()
							elseif seat == 'pass' then
							ESX.ShowNotification(Config.FrontArmed)
								seatBomCars[2] = 0
								menu.close()
							elseif seat == 'backdrive' then
								ESX.ShowNotification(Config.BackLeft)
								seatBomCars[2] = 1
								menu.close()
							elseif seat == 'backpass' then
								ESX.ShowNotification(Config.BackRight)
								seatBomCars[2] = 2
								menu.close()
							elseif seat == 'farbackdrive' then
								ESX.ShowNotification(Config.FarLeft)
								seatBomCars[2] = 3
								menu.close()
							elseif seat == 'farbackpass' then
								ESX.ShowNotification(Config.FarRight)
								seatBomCars[2] = 4
								menu.close()
							else
								ESX.ShowNotification(Config.WongSeat)
							end
						end, function(data, menu)
						end)
					else
						ESX.ShowNotification(Config.OneBomb)
						OpenDisarmMenu(ped, 'seat')
					end
					menu.close()
				end
			end, function(data, menu)
				menu.close()
			end)
		else
			ESX.ShowNotification(Config.FaceCar)
		end
	else
		ESX.UI.Menu.CloseAll()
		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'reverse_engineer', {
			title    = Config.MakeDeff,
			align    = 'top-left',
			elements = {
				{label = Config.RevToDeff, value = 'reveng'},
				{label = Config.Deny, value = 'cancel'}
			}
		}, function(data, menu)
			if data.current.value == 'reveng' then
				TriggerServerEvent('esx_carBomb:createDefuser')
				menu.close()
			else
				menu.close()
			end
		end, function(data, menu)
		end)
	end
end)

AddEventHandler('esx:playerLoaded', function()
	TriggerServerEvent('esx_carBomb:sendCars')
end)

RegisterNetEvent('esx_carBomb:trespass')
AddEventHandler('esx_carBomb:trespass', function(ped)
    local x,y,z = table.unpack(GetEntityCoords(ped, false))
    local plyPos = GetEntityCoords(ped,  true)
    local streetName, crossing = Citizen.InvokeNative( 0x2EB41072B4C1E4C0, plyPos.x, plyPos.y, plyPos.z, Citizen.PointerValueInt(), Citizen.PointerValueInt() )
    local streetName, crossing = GetStreetNameAtCoord(x, y, z)
    streetName = GetStreetNameFromHashKey(streetName)
    crossing = GetStreetNameFromHashKey(crossing)
	
	if Config.UseGCPhone then
        if crossing ~= nil then
            local coords      = GetEntityCoords(ped)

            TriggerServerEvent('esx_addons_gcphone:startCall', 'police', Config.Intruder .. streetName .. ' and ' .. crossing, PlayerCoords, {
                PlayerCoords = { x = PedPosition.x, y = PedPosition.y, z = PedPosition.z },
            })
        else
            TriggerServerEvent('esx_addons_gcphone:startCall', 'police', Config.Intruder .. streetName, PlayerCoords, {
                PlayerCoords = { x = PedPosition.x, y = PedPosition.y, z = PedPosition.z },
            })
        end
    else
		TriggerServerEvent('esx_carBomb:policeAlert', streetName)
	end
end)

Citizen.CreateThread(function()
	while true do
		local ped = PlayerPedId()
		Citizen.Wait(1000)
		if Config.JobOnly then
			for i = 1,#Config.Jobs do
				if PlayerData.job.name == Config.Jobs[i] then
					if ticketTime ~= 0 then
						hasTicket = true
						ticketTime = ticketTime - 1
						if ticketTime == 30 then
							ESX.ShowNotification(Config.TimeLeft)
						end
					else
						hasTicket = false
						showing = false
					end
					local coords = GetEntityCoords(ped)
					for i = 1,#servBomCars do
						local carpos = GetEntityCoords(servBomCars[i])
						if enBomCars[1] ~= nil then
							if enBomCars[1] == servBomCars[i] then
								local enBomCar = enBomCars[1]
								local id = NetworkGetNetworkIdFromEntity(enBomCar)
								SetNetworkIdCanMigrate(id, true)
								local enCarPos = GetEntityCoords(enBomCar)
								local engineOn = GetIsVehicleEngineRunning(enBomCar)
								if engineOn ~= false then
									TriggerServerEvent('esx_carBomb:ensureEsplode', id)
									SetEntityAsNoLongerNeeded(enBomCar)
									enBomCars[1] = nil
									ESX.ShowNotification(Config.Triggered)
								end
							end
						end
						if poBomCars[1] ~= nil then
							if poBomCars[1] == servBomCars[i] then
								local poBomCar = poBomCars[1]
								local id = NetworkGetNetworkIdFromEntity(poBomCar)
								SetNetworkIdCanMigrate(id, true)
								local poCarPos = GetEntityCoords(poBomCar)
								local startPos = poBomCars[2]
								if poBomCars[3] ~= nil then
									if GetDistanceBetweenCoords(poCarPos, startPos, true) > poBomCars[3] then
										TriggerServerEvent('esx_carBomb:ensureEsplode', id)
										NetworkExplodeVehicle(poBomCar, true, false, 0)
										ExplodeVehicle(poBombCar, true, false)
										AddExplosion(poCarPos, 7, 2000, true, false, 50, false)
										SetEntityAsNoLongerNeeded(poBomCar)
										poBomCars[1] = nil
										poBomCars[2] = nil
										poBomCars[3] = nil
										ESX.ShowNotification(Config.Triggered)
									end
								end
							end
						end
						if msBomCars[1] ~= nil then
							if msBomCars[1] == servBomCars[i] then
								local msBomCar = msBomCars[1]
								local id = NetworkGetNetworkIdFromEntity(msBomCar)
								SetNetworkIdCanMigrate(id, true)
								local msCarPos = GetEntityCoords(msBomCar)
								local speed = GetEntitySpeed(msBomCar)
								local mscSpeed
								if Config.UseKPH == true then
									mscSpeed = (speed * 3.6)
								else
									mscSpeed = (speed * 2.236936)
								end
								if msBomCars[2] ~= nil then
									if mscSpeed > msBomCars[2] then
										TriggerServerEvent('esx_carBomb:ensureEsplode', id)
										SetEntityAsNoLongerNeeded(msBomCar)
										msBomCars[1] = nil
										msBomCars[2] = nil
										ESX.ShowNotification(Config.Triggered)
									end
								end
							end
						end
						if stopBomCars[1] ~= nil then
							if stopBomCars[1] == servBomCars[i] then
								local stopBomCar = stopBomCars[1]
								local id = NetworkGetNetworkIdFromEntity(stopBomCar)
								SetNetworkIdCanMigrate(id, true)
								local stopCarPos = GetEntityCoords(stopBomCar)
								local startPos = stopBomCars[2]
								local stopcSpeed = GetEntitySpeed(stopBomCar)
								if stopBomCars[3] ~= nil then
									if GetDistanceBetweenCoords(stopCarPos, startPos, true) > stopBomCars[3] then
										if stopcSpeed == 0 then
											TriggerServerEvent('esx_carBomb:ensureEsplode', id)
											SetEntityAsNoLongerNeeded(stopBomCar)
											stopBomCars[1] = nil
											stopBomCars[2] = nil
											stopBomCars[3] = nil
											ESX.ShowNotification(Config.Triggered)
										end
									end
								end
							end
						end
						if seatBomCars[1] ~= nil then
							if seatBomCars[1] == servBomCars[i] then
								local seatBomCar = seatBomCars[1]
								local id = NetworkGetNetworkIdFromEntity(seatBomCar)
								SetNetworkIdCanMigrate(id, true)
								local seatCarPos = GetEntityCoords(seatBomCar)
								if seatBomCars[2] ~= nil then
									local empty = IsVehicleSeatFree(seatBomCar, seatBomCars[2])
									if empty == false then
										TriggerServerEvent('esx_carBomb:ensureEsplode', id)
										SetEntityAsNoLongerNeeded(seatBomCar)
										seatBomCars[1] = nil
										seatBomCars[2] = nil
										ESX.ShowNotification(Config.Triggered)
									end
								end
							end
						end
					end
				end
			end
		else
			if ticketTime ~= 0 then
				hasTicket = true
				ticketTime = ticketTime - 1
				if ticketTime == 30 then
					ESX.ShowNotification(Config.TimeLeft)
				end
			else
				hasTicket = false
				showing = false
			end
			local coords = GetEntityCoords(ped)
			for i = 1,#servBomCars do
				local carpos = GetEntityCoords(servBomCars[i])
				if enBomCars[1] ~= nil then
					if enBomCars[1] == servBomCars[i] then
						local enBomCar = enBomCars[1]
						local id = NetworkGetNetworkIdFromEntity(enBomCar)
						SetNetworkIdCanMigrate(id, true)
						local enCarPos = GetEntityCoords(enBomCar)
						local engineOn = GetIsVehicleEngineRunning(enBomCar)
						if engineOn ~= false then
							TriggerServerEvent('esx_carBomb:ensureEsplode', id)
							SetEntityAsNoLongerNeeded(enBomCar)
							enBomCars[1] = nil
							ESX.ShowNotification(Config.Triggered)
						end
					end
				end
				if poBomCars[1] ~= nil then
					if poBomCars[1] == servBomCars[i] then
						local poBomCar = poBomCars[1]
						local id = NetworkGetNetworkIdFromEntity(poBomCar)
						SetNetworkIdCanMigrate(id, true)
						local poCarPos = GetEntityCoords(poBomCar)
						local startPos = poBomCars[2]
						if poBomCars[3] ~= nil then
							if GetDistanceBetweenCoords(poCarPos, startPos, true) > poBomCars[3] then
								TriggerServerEvent('esx_carBomb:ensureEsplode', id)
								NetworkExplodeVehicle(poBomCar, true, false, 0)
								ExplodeVehicle(poBombCar, true, false)
								AddExplosion(poCarPos, 7, 2000, true, false, 50, false)
								SetEntityAsNoLongerNeeded(poBomCar)
								poBomCars[1] = nil
								poBomCars[2] = nil
								poBomCars[3] = nil
								ESX.ShowNotification(Config.Triggered)
							end
						end
					end
				end
				if msBomCars[1] ~= nil then
					if msBomCars[1] == servBomCars[i] then
						local msBomCar = msBomCars[1]
						local id = NetworkGetNetworkIdFromEntity(msBomCar)
						SetNetworkIdCanMigrate(id, true)
						local msCarPos = GetEntityCoords(msBomCar)
						local speed = GetEntitySpeed(msBomCar)
						local mscSpeed
						if Config.UseKPH == true then
							mscSpeed = (speed * 3.6)
						else
							mscSpeed = (speed * 2.236936)
						end
						if msBomCars[2] ~= nil then
							if mscSpeed > msBomCars[2] then
								TriggerServerEvent('esx_carBomb:ensureEsplode', id)
								SetEntityAsNoLongerNeeded(msBomCar)
								msBomCars[1] = nil
								msBomCars[2] = nil
								ESX.ShowNotification(Config.Triggered)
							end
						end
					end
				end
				if stopBomCars[1] ~= nil then
					if stopBomCars[1] == servBomCars[i] then
						local stopBomCar = stopBomCars[1]
						local id = NetworkGetNetworkIdFromEntity(stopBomCar)
						SetNetworkIdCanMigrate(id, true)
						local stopCarPos = GetEntityCoords(stopBomCar)
						local startPos = stopBomCars[2]
						local stopcSpeed = GetEntitySpeed(stopBomCar)
						if stopBomCars[3] ~= nil then
							if GetDistanceBetweenCoords(stopCarPos, startPos, true) > stopBomCars[3] then
								if stopcSpeed == 0 then
									TriggerServerEvent('esx_carBomb:ensureEsplode', id)
									SetEntityAsNoLongerNeeded(stopBomCar)
									stopBomCars[1] = nil
									stopBomCars[2] = nil
									stopBomCars[3] = nil
									ESX.ShowNotification(Config.Triggered)
								end
							end
						end
					end
				end
				if seatBomCars[1] ~= nil then
					if seatBomCars[1] == servBomCars[i] then
						local seatBomCar = seatBomCars[1]
						local id = NetworkGetNetworkIdFromEntity(seatBomCar)
						SetNetworkIdCanMigrate(id, true)
						local seatCarPos = GetEntityCoords(seatBomCar)
						if seatBomCars[2] ~= nil then
							local empty = IsVehicleSeatFree(seatBomCar, seatBomCars[2])
							if empty == false then
								TriggerServerEvent('esx_carBomb:ensureEsplode', id)
								SetEntityAsNoLongerNeeded(seatBomCar)
								seatBomCars[1] = nil
								seatBomCars[2] = nil
								ESX.ShowNotification(Config.Triggered)
							end
						end
					end
				end
			end
		end
	end
end)

RegisterNetEvent('esx_carBomb:ensureEsplode')
AddEventHandler('esx_carBomb:ensureEsplode', function(id)
	
	local ped = PlayerPedId()
	local car = GetVehiclePedIsIn(ped)
	local cmod = GetEntityModel(car)
	local veh = NetworkGetEntityFromNetworkId(id)
	local vmod = GetEntityModel(veh)
	local pos = GetEntityCoords(veh)
	SetEntityAsNoLongerNeeded(veh)
	SetEntityCanBeDamaged(veh, true)
	SetEntityInvincible(veh, false)
	SetEntityProofs(veh, false, false, false, false, false, false, false, false)
	SetVehicleDamage(veh, 0.0, 0.0, 0.0, 5000, 2, true)
	NetworkExplodeVehicle(veh, true, false, 0)
	ExplodeVehicle(veh, true, false)
	AddExplosion(pos, 7, 2000, true, false, 50, false)
end)

Citizen.CreateThread(function()

	if Config.JobOnly then
		for i = 1,#Config.Jobs do
			while PlayerData == nil do
				Citizen.Wait(10)
			end
			if PlayerData.job.name == Config.Jobs[i] then
				for loc,areas in pairs(Config.Zones.CollectZones) do
					local blip
					if loc == 'RecCenter' then
						blip = AddBlipForRadius(areas.Yard, 25.0)

						SetBlipColour(blip, 1)
						SetBlipAlpha(blip, 100)
						SetBlipDisplay(blip, 5)
						SetBlipAsShortRange(blip, true)
					elseif loc == 'HayesAuto' then
						blip = AddBlipForRadius(areas.Yard, 15.0)

						SetBlipColour(blip, 1)
						SetBlipAlpha(blip, 100)
						SetBlipDisplay(blip, 5)
						SetBlipAsShortRange(blip, true)
					else
						blip = AddBlipForRadius(areas.Yard, 20.0)

						SetBlipColour(blip, 1)
						SetBlipAlpha(blip, 100)
						SetBlipDisplay(blip, 5)
						SetBlipAsShortRange(blip, true)
					end
					blip = AddBlipForCoord(areas.Entrance)
					
					SetBlipSprite (blip, 276)
					SetBlipDisplay(blip, 4)
					SetBlipScale  (blip, 1.0)
					SetBlipColour (blip, 2)
					SetBlipAsShortRange(blip, true)
					BeginTextCommandSetBlipName('STRING')
					AddTextComponentString(loc)
					EndTextCommandSetBlipName(blip)
				end
				while true do
					local ped = PlayerPedId()
					local sleep = 500
					local coords = GetEntityCoords(ped)
					local dis
					for k,v in pairs(Config.Zones) do
						for loc,areas in pairs(v) do
							if loc == 'SandyAirField' or loc == 'RecCenter' or loc == 'HayesAuto' or loc == 'MuriettaOilField' then
								dis = #(coords - areas.Entrance)
								if dis < Config.DrawDistance then
									sleep = 10
									DrawMarker(29, areas.Entrance, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.5, 0.5, 50, 250, 100, 200, false, true, 2, false, 0, 0, false)
								end
								if dis < 1.5 then
									ESX.ShowHelpNotification(Config.ScrapInteract)
									if IsControlJustReleased(0, 51) then
										local elements = {
											{label = Config.BuyTick, value = 'buy'},
											{label = Config.GiveTrash, value = 'give'}
										}
										ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'scrap_yard', {
											title    = Config.ScrapYard,
											align    = 'top-left',
											elements = elements
										}, function(data, menu)
											if data.current.value == 'buy' then
												TriggerServerEvent('esx_carBomb:sellTicket')
												menu.close()
											elseif data.current.value == 'give' then
												TriggerServerEvent('esx_carBomb:takeTrash')
												menu.close()
											end
										end, function(data, menu)
											menu.close()
										end)
									end
								end
							else
								dis = #(coords - areas)
								if dis < Config.DrawDistance then
									sleep = 10
									DrawMarker(0, areas, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.5, 0.5, 50, 250, 100, 200, false, true, 2, false, 0, 0, false)
								end
								if dis < 1.5 then
									if k == 'ProcessZones' then
										if working == false then
											ESX.ShowHelpNotification(Config.CraftBomb)
											if IsControlJustReleased(0, 51) then
												TriggerServerEvent('esx_carBomb:createBomb', ped, coords)
											end
										end
									elseif k == 'TeleInZones' then
										ESX.ShowHelpNotification(Config.EnterZone)
										if IsControlJustReleased(0, 51) then
											for nloc,nareas in pairs(Config.Zones.TeleOutZones) do
												if nloc == loc then
													SetEntityCoords(ped, nareas, false, false, false, false)
												end
											end
										end
									elseif k == 'TeleOutZones' then
										ESX.ShowHelpNotification(Config.ExitZone)
										if IsControlJustReleased(0, 51) then
											for nloc,nareas in pairs(Config.Zones.TeleInZones) do
												if nloc == loc then
													SetEntityCoords(ped, nareas, false, false, false, false)
												end
											end
										end
									end
								end
							end
						end
					end
					if GetDistanceBetweenCoords(coords, Config.Zones.CollectZones.RecCenter.Yard, true) < 25 then
						if hasTicket == false then
							if fighting == false then
								RequestModel(GetHashKey('a_m_m_hillbilly_01'))
								while not HasModelLoaded('a_m_m_hillbilly_01') do
									Citizen.Wait(1)
								end
								npc = CreatePed(0, GetHashKey('a_m_m_hillbilly_01'), Config.Zones.CollectZones.RecCenter.Yard, 100.0, true, false)
								npcT[1] = npc
								SetPedAsEnemy(npc, true)
								SetEntityInvincible(npc, true)
								TaskCombatPed(npc, ped, 0, 16)
								SetRelationshipBetweenGroups(5, -1285976420, GetHashKey('PLAYER'))
								SetRelationshipBetweenGroups(5, GetHashKey('PLAYER'), -1285976420)
								if Config.AlertCops then
									TriggerEvent('esx_carBomb:trespass', ped)
								end
								fighting = true
							end
						else
							if showing == false then
								ESX.ShowNotification(Config.Search)
								SetRelationshipBetweenGroups(1, -1285976420, GetHashKey('PLAYER'))
								SetRelationshipBetweenGroups(1, GetHashKey('PLAYER'), -1285976420)
								showing = true
							else
								Search(ped, coords)
							end
						end
					elseif GetDistanceBetweenCoords(coords, Config.Zones.CollectZones.SandyAirField.Yard, true) < 20 then
						if hasTicket == false then
							if fighting == false then
								RequestModel(GetHashKey('a_m_m_hillbilly_01'))
								while not HasModelLoaded('a_m_m_hillbilly_01') do
									Citizen.Wait(1)
								end
								npc = CreatePed(0, GetHashKey('a_m_m_hillbilly_01'), Config.Zones.CollectZones.SandyAirField.Yard, 100.0, true, false)
								npcT[1] = npc
								SetPedAsEnemy(npc, true)
								SetEntityInvincible(npc, true)
								TaskCombatPed(npc, ped, 0, 16)
								SetRelationshipBetweenGroups(5, -1285976420, GetHashKey('PLAYER'))
								SetRelationshipBetweenGroups(5, GetHashKey('PLAYER'), -1285976420)
								if Config.AlertCops then
									TriggerEvent('esx_carBomb:trespass', ped)
								end
								fighting = true
							end
						else
							if showing == false then
								ESX.ShowNotification(Config.Search)
								SetRelationshipBetweenGroups(1, -1285976420, GetHashKey('PLAYER'))
								SetRelationshipBetweenGroups(1, GetHashKey('PLAYER'), -1285976420)
								showing = true
							else
								Search(ped, coords)
							end
						end
					elseif GetDistanceBetweenCoords(coords, Config.Zones.CollectZones.MuriettaOilField.Yard, true) < 25 then
						if hasTicket == false then
							if fighting == false then
								RequestModel(GetHashKey('a_m_m_hillbilly_01'))
								while not HasModelLoaded('a_m_m_hillbilly_01') do
									Citizen.Wait(1)
								end
								npc = CreatePed(0, GetHashKey('a_m_m_hillbilly_01'), Config.Zones.CollectZones.MuriettaOilField.Yard, 100.0, true, false)
								npcT[1] = npc
								SetPedAsEnemy(npc, true)
								SetEntityInvincible(npc, true)
								TaskCombatPed(npc, ped, 0, 16)
								SetRelationshipBetweenGroups(5, -1285976420, GetHashKey('PLAYER'))
								SetRelationshipBetweenGroups(5, GetHashKey('PLAYER'), -1285976420)
								if Config.AlertCops then
									TriggerEvent('esx_carBomb:trespass', ped)
								end
								fighting = true
							end
						else
							if showing == false then
								ESX.ShowNotification(Config.Search)
								SetRelationshipBetweenGroups(1, -1285976420, GetHashKey('PLAYER'))
								SetRelationshipBetweenGroups(1, GetHashKey('PLAYER'), -1285976420)
								showing = true
							else
								Search(ped, coords)
							end
						end
					elseif GetDistanceBetweenCoords(coords, Config.Zones.CollectZones.HayesAuto.Yard, true) < 15 then
						if hasTicket == false then
							if fighting == false then
								RequestModel(GetHashKey('a_m_m_hillbilly_01'))
								while not HasModelLoaded('a_m_m_hillbilly_01') do
									Citizen.Wait(1)
								end
								npc = CreatePed(0, GetHashKey('a_m_m_hillbilly_01'), Config.Zones.CollectZones.HayesAuto.Yard, 100.0, true, false)
								npcT[1] = npc
								SetPedAsEnemy(npc, true)
								SetEntityInvincible(npc, true)
								TaskCombatPed(npc, ped, 0, 16)
								SetRelationshipBetweenGroups(5, -1285976420, GetHashKey('PLAYER'))
								SetRelationshipBetweenGroups(5, GetHashKey('PLAYER'), -1285976420)
								if Config.AlertCops then
									TriggerEvent('esx_carBomb:trespass', ped)
								end
								fighting = true
							end
						else
							if showing == false then
								ESX.ShowNotification(Config.Search)
								SetRelationshipBetweenGroups(1, -1285976420, GetHashKey('PLAYER'))
								SetRelationshipBetweenGroups(1, GetHashKey('PLAYER'), -1285976420)
								showing = true
							else
								Search(ped, coords)
							end
						end
					else
						if fighting == true then
							npc = npcT[1]
							SetPedAsEnemy(npc, false)
							SetEntityInvincible(npc, false)
							ClearPedTasks(npc)
							SetRelationshipBetweenGroups(1, -1285976420, GetHashKey('PLAYER'))
							SetRelationshipBetweenGroups(1, GetHashKey('PLAYER'), -1285976420)
							SetEntityAsNoLongerNeeded(npc)
							fighting = false
						end
						if showing == true then
							showing = false
						end
					end
					Citizen.Wait(sleep)
				end
			end
		end
	else
		for loc,areas in pairs(Config.Zones.CollectZones) do
			local blip
			if loc == 'RecCenter' then
				blip = AddBlipForRadius(areas.Yard, 25.0)

				SetBlipColour(blip, 1)
				SetBlipAlpha(blip, 100)
				SetBlipDisplay(blip, 5)
				SetBlipAsShortRange(blip, true)
			elseif loc == 'HayesAuto' then
				blip = AddBlipForRadius(areas.Yard, 15.0)

				SetBlipColour(blip, 1)
				SetBlipAlpha(blip, 100)
				SetBlipDisplay(blip, 5)
				SetBlipAsShortRange(blip, true)
			else
				blip = AddBlipForRadius(areas.Yard, 20.0)

				SetBlipColour(blip, 1)
				SetBlipAlpha(blip, 100)
				SetBlipDisplay(blip, 5)
				SetBlipAsShortRange(blip, true)
			end
			blip = AddBlipForCoord(areas.Entrance)
			
			SetBlipSprite (blip, 276)
			SetBlipDisplay(blip, 4)
			SetBlipScale  (blip, 1.0)
			SetBlipColour (blip, 2)
			SetBlipAsShortRange(blip, true)
			BeginTextCommandSetBlipName('STRING')
			AddTextComponentString(loc)
			EndTextCommandSetBlipName(blip)
		end
		while true do
			local ped = PlayerPedId()
			local sleep = 500
			local coords = GetEntityCoords(ped)
			local dis
			for k,v in pairs(Config.Zones) do
				for loc,areas in pairs(v) do
					if loc == 'SandyAirField' or loc == 'RecCenter' or loc == 'HayesAuto' or loc == 'MuriettaOilField' then
						dis = #(coords - areas.Entrance)
						if dis < Config.DrawDistance then
							sleep = 10
							DrawMarker(29, areas.Entrance, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.5, 0.5, 50, 250, 100, 200, false, true, 2, false, 0, 0, false)
						end
						if dis < 1.5 then
							ESX.ShowHelpNotification(Config.ScrapInteract)
							if IsControlJustReleased(0, 51) then
								local elements = {
									{label = Config.BuyTick, value = 'buy'},
									{label = Config.GiveTrash, value = 'give'}
								}
								ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'scrap_yard', {
									title    = Config.ScrapYard,
									align    = 'top-left',
									elements = elements
								}, function(data, menu)
									if data.current.value == 'buy' then
										TriggerServerEvent('esx_carBomb:sellTicket')
										menu.close()
									elseif data.current.value == 'give' then
										TriggerServerEvent('esx_carBomb:takeTrash')
										menu.close()
									end
								end, function(data, menu)
									menu.close()
								end)
							end
						end
					else
						dis = #(coords - areas)
						if dis < Config.DrawDistance then
							DrawMarker(0, areas, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.5, 0.5, 50, 250, 100, 200, false, true, 2, false, 0, 0, false)
						end
						if dis < 1.5 then
							if k == 'ProcessZones' then
								if working == false then
									ESX.ShowHelpNotification(Config.CraftBomb)
									if IsControlJustReleased(0, 51) then
										TriggerServerEvent('esx_carBomb:createBomb', ped, coords)
									end
								end
							elseif k == 'TeleInZones' then
								ESX.ShowHelpNotification(Config.EnterZone)
								if IsControlJustReleased(0, 51) then
									for nloc,nareas in pairs(Config.Zones.TeleOutZones) do
										if nloc == loc then
											SetEntityCoords(ped, nareas, false, false, false, false)
										end
									end
								end
							elseif k == 'TeleOutZones' then
								ESX.ShowHelpNotification(Config.ExitZone)
								if IsControlJustReleased(0, 51) then
									for nloc,nareas in pairs(Config.Zones.TeleInZones) do
										if nloc == loc then
											SetEntityCoords(ped, nareas, false, false, false, false)
										end
									end
								end
							end
						end
					end
				end
			end
			if GetDistanceBetweenCoords(coords, Config.Zones.CollectZones.RecCenter.Yard, true) < 25 then
				if hasTicket == false then
					if fighting == false then
						RequestModel(GetHashKey('a_m_m_hillbilly_01'))
						while not HasModelLoaded('a_m_m_hillbilly_01') do
							Citizen.Wait(1)
						end
						npc = CreatePed(0, GetHashKey('a_m_m_hillbilly_01'), Config.Zones.CollectZones.RecCenter.Yard, 100.0, true, false)
						npcT[1] = npc
						SetPedAsEnemy(npc, true)
						SetEntityInvincible(npc, true)
						TaskCombatPed(npc, ped, 0, 16)
						SetRelationshipBetweenGroups(5, -1285976420, GetHashKey('PLAYER'))
						SetRelationshipBetweenGroups(5, GetHashKey('PLAYER'), -1285976420)
						if Config.AlertCops then
							TriggerEvent('esx_carBomb:trespass', ped)
						end
						fighting = true
					end
				else
					if showing == false then
						ESX.ShowNotification(Config.Search)
						SetRelationshipBetweenGroups(1, -1285976420, GetHashKey('PLAYER'))
						SetRelationshipBetweenGroups(1, GetHashKey('PLAYER'), -1285976420)
						showing = true
					else
						Search(ped, coords)
					end
				end
			elseif GetDistanceBetweenCoords(coords, Config.Zones.CollectZones.SandyAirField.Yard, true) < 20 then
				if hasTicket == false then
					if fighting == false then
						RequestModel(GetHashKey('a_m_m_hillbilly_01'))
						while not HasModelLoaded('a_m_m_hillbilly_01') do
							Citizen.Wait(1)
						end
						npc = CreatePed(0, GetHashKey('a_m_m_hillbilly_01'), Config.Zones.CollectZones.SandyAirField.Yard, 100.0, true, false)
						npcT[1] = npc
						SetPedAsEnemy(npc, true)
						SetEntityInvincible(npc, true)
						TaskCombatPed(npc, ped, 0, 16)
						SetRelationshipBetweenGroups(5, -1285976420, GetHashKey('PLAYER'))
						SetRelationshipBetweenGroups(5, GetHashKey('PLAYER'), -1285976420)
						if Config.AlertCops then
							TriggerEvent('esx_carBomb:trespass', ped)
						end
						fighting = true
					end
				else
					if showing == false then
						ESX.ShowNotification(Config.Search)
						SetRelationshipBetweenGroups(1, -1285976420, GetHashKey('PLAYER'))
						SetRelationshipBetweenGroups(1, GetHashKey('PLAYER'), -1285976420)
						showing = true
					else
						Search(ped, coords)
					end
				end
			elseif GetDistanceBetweenCoords(coords, Config.Zones.CollectZones.MuriettaOilField.Yard, true) < 25 then
				if hasTicket == false then
					if fighting == false then
						RequestModel(GetHashKey('a_m_m_hillbilly_01'))
						while not HasModelLoaded('a_m_m_hillbilly_01') do
							Citizen.Wait(1)
						end
						npc = CreatePed(0, GetHashKey('a_m_m_hillbilly_01'), Config.Zones.CollectZones.MuriettaOilField.Yard, 100.0, true, false)
						npcT[1] = npc
						SetPedAsEnemy(npc, true)
						SetEntityInvincible(npc, true)
						TaskCombatPed(npc, ped, 0, 16)
						SetRelationshipBetweenGroups(5, -1285976420, GetHashKey('PLAYER'))
						SetRelationshipBetweenGroups(5, GetHashKey('PLAYER'), -1285976420)
						if Config.AlertCops then
							TriggerEvent('esx_carBomb:trespass', ped)
						end
						fighting = true
					end
				else
					if showing == false then
						ESX.ShowNotification(Config.Search)
						SetRelationshipBetweenGroups(1, -1285976420, GetHashKey('PLAYER'))
						SetRelationshipBetweenGroups(1, GetHashKey('PLAYER'), -1285976420)
						showing = true
					else
						Search(ped, coords)
					end
				end
			elseif GetDistanceBetweenCoords(coords, Config.Zones.CollectZones.HayesAuto.Yard, true) < 15 then
				if hasTicket == false then
					if fighting == false then
						RequestModel(GetHashKey('a_m_m_hillbilly_01'))
						while not HasModelLoaded('a_m_m_hillbilly_01') do
							Citizen.Wait(1)
						end
						npc = CreatePed(0, GetHashKey('a_m_m_hillbilly_01'), Config.Zones.CollectZones.HayesAuto.Yard, 100.0, true, false)
						npcT[1] = npc
						SetPedAsEnemy(npc, true)
						SetEntityInvincible(npc, true)
						TaskCombatPed(npc, ped, 0, 16)
						SetRelationshipBetweenGroups(5, -1285976420, GetHashKey('PLAYER'))
						SetRelationshipBetweenGroups(5, GetHashKey('PLAYER'), -1285976420)
						if Config.AlertCops then
							TriggerEvent('esx_carBomb:trespass', ped)
						end
						fighting = true
					end
				else
					if showing == false then
						ESX.ShowNotification(Config.Search)
						SetRelationshipBetweenGroups(1, -1285976420, GetHashKey('PLAYER'))
						SetRelationshipBetweenGroups(1, GetHashKey('PLAYER'), -1285976420)
						showing = true
					else
						Search(ped, coords)
					end
				end
			else
				if fighting == true then
					npc = npcT[1]
					SetPedAsEnemy(npc, false)
					SetEntityInvincible(npc, false)
					ClearPedTasks(npc)
					SetRelationshipBetweenGroups(1, -1285976420, GetHashKey('PLAYER'))
					SetRelationshipBetweenGroups(1, GetHashKey('PLAYER'), -1285976420)
					SetEntityAsNoLongerNeeded(npc)
					fighting = false
				end
				if showing == true then
					showing = false
				end
			end
			Citizen.Wait(sleep)
		end
	end
end)

AddEventHandler('onResourceStop', function(resource)
	if resource == GetCurrentResourceName() then
		ESX.UI.Menu.CloseAll()
		npc = npcT[1]
		SetPedAsEnemy(npc, false)
		SetEntityInvincible(npc, false)
		ClearPedTasks(npc)
		SetRelationshipBetweenGroups(1, -1285976420, GetHashKey('PLAYER'))
		SetRelationshipBetweenGroups(1, GetHashKey('PLAYER'), -1285976420)
		SetEntityAsNoLongerNeeded(npc)
	end
end)